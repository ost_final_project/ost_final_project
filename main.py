#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import cgi
import datetime
import time
from time import *
from datetime import datetime,timedelta
from calendar import timegm
from google.appengine.ext import db
from google.appengine.api import users
import webapp2
import os
import urllib
import uuid
from google.appengine.ext.db import *
from google.appengine.api import users
from google.appengine.ext import ndb
from operator import attrgetter

import jinja2





JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)



def Resource1_key(resource_key):

    return ndb.Key('resource_key', datetime.date.today().strftime("%B %d, %Y"))
def reserve1_key(reserve_key):
	user = users.get_current_user()
	return ndb.Key('reserve_key',user.nickname())

class Resource(ndb.Model):
	Resource_name = ndb.StringProperty(indexed=True)
	Resource_ID = ndb.StringProperty(indexed=True)
	Resource_tags = ndb.StringProperty(indexed=True,repeated =True)
	Resource_owner = ndb.StringProperty(indexed=True)
	Resource_start = ndb.StringProperty(indexed=False)
	Resource_end = ndb.StringProperty(indexed=False)
	Resource_counter = ndb.DateTimeProperty(indexed=False)
	#Resource_capacity = ndb.IntegerProperty(indexed = False)
	Resource_description = ndb.StringProperty(indexed = False)

	
	
class Reservation(ndb.Model):
	Reservation_ID = ndb.StringProperty(indexed=True)
	User_ID = ndb.StringProperty(indexed=True)
	Resource_ID = ndb.StringProperty(indexed=True)
	Reservation_from = ndb.StringProperty(indexed=False)
	Reservation_duration = ndb.StringProperty(indexed=False)
	Reservation_endtime = ndb.StringProperty(indexed=False)
	Resource_name = ndb.StringProperty(indexed=False)
	
class Resource_handler(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				resource_Id = self.request.get('Resource_ID')
				query_1 = Resource.gql("WHERE Resource_ID = :1",resource_Id)
				list_2 = list(query_1)
				
				
				
				query_2 = Reservation.gql("WHERE Resource_ID = :1",resource_Id)
				new_list = list(query_2)
				length = len(new_list)
				list_1 = []
				current_time = datetime.datetime.now()+ datetime.timedelta(hours = -5)
				for item in new_list:
					if(datetime.datetime.strptime(item.Reservation_endtime,'%Y-%m-%d %H:%M') > current_time):
						list_1.append(item)
				
				
				
				template_values = { 'list1' : list_1, 'rid' : list_2[0] , 'length' : length }
				template = JINJA_ENVIRONMENT.get_template('resource.html')
			
			
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))

				
class Reservation_handler(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				resource_Id = self.request.get('Resource_ID')
				q = Resource.gql("WHERE Resource_ID = :1",resource_Id)
				new_list = list(q)
				template_values = { 'rid' : new_list[0] }
				
				template = JINJA_ENVIRONMENT.get_template('Reserve_resource.html')
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))
		
		def post(self):
			
			var = True
			
			start_time = self.request.get('start_time')
			pattern = '%Y-%m-%d %H:%M'
			duration = self.request.get('duration')


			try:
				start = datetime.datetime.strptime(start_time,pattern)
				start_date =datetime.datetime.strptime(start_time,pattern).date()
				dur = datetime.datetime.strptime(duration,'%H:%M')
				t = datetime.timedelta(minutes = dur.minute) + datetime.timedelta(hours =dur.hour)
				end_time1 = start + t
				end_time = end_time1.strftime(pattern)
				
			except ValueError:
				resource_Id = self.request.get('uid')
				qu = Resource.gql("WHERE Resource_ID = :1",resource_Id)
				new_list = list(qu)
				template_values = { 'error': "The start time/duration is invalid" ,'rid' : new_list[0]}
				template = JINJA_ENVIRONMENT.get_template('Reserve_resource.html')
				self.response.write(template.render(template_values))
				return
				
			end_1 = datetime.datetime.strptime(end_time,pattern).date()	
			#self.response.write(start_date)
			#self.response.write(end_1)
			if(end_1 > start_date):
				resource_Id = self.request.get('uid')
				qu = Resource.gql("WHERE Resource_ID = :1",resource_Id)
				new_list = list(qu)
				template_values = { 'error': "The reservation cannot span a day boundary" ,'rid' : new_list[0]}
				template = JINJA_ENVIRONMENT.get_template('Reserve_resource.html')
				self.response.write(template.render(template_values))
			else:	
				user = users.get_current_user()
				reserve_key = self.request.get('reserve_key')
				reserve = Reservation(parent=reserve1_key(reserve_key))
				reserve.User_ID = user.nickname()
				reserve.Reservation_from = self.request.get('start_time')
				reserve.Reservation_duration = self.request.get('duration')
				reserve.Reservation_ID = str(uuid.uuid1())
				reserve.Resource_ID = self.request.get('uid')
				reserve.Reservation_endtime = end_time
				reserve.Resource_name = self.request.get('name')
				res_start = self.request.get('start')
				res_end = self.request.get('end')

				Resourcestart = datetime.datetime.strptime(res_start,'%H:%M').time()
				Res_start1 = datetime.datetime.strptime(start_time,pattern).time()
				Reservation_start = datetime.datetime.strptime(start_time,pattern)
				current_time = datetime.datetime.now()+ datetime.timedelta(hours = -5)
				Resourceend = datetime.datetime.strptime(res_end,'%H:%M').time()
				Res_end1 = datetime.datetime.strptime(end_time,pattern).time()
				#self.response.write(Res_end1)
				Reservation_end = datetime.datetime.strptime(end_time,pattern)
				if(Resourcestart <= Res_start1) and (Resourceend >= Res_end1) and (Reservation_start >= current_time):
				
					user_reserves = Reservation.gql("WHERE User_ID =:1",user.nickname())
					user_list = list(user_reserves)
					counter=0
					for items in user_list:
						user_reserved_from = datetime.datetime.strptime(items.Reservation_from,pattern)
						user_reserved_till = datetime.datetime.strptime(items.Reservation_endtime,pattern)	
						if(Reservation_start > user_reserved_till) or (Reservation_end < user_reserved_from):	
							counter +=1
					if(counter == len(user_list)):
					#self.response.write("Reservation Can Be Done because User is Free")

						resource_Id = self.request.get('uid')
						list_1 = Reservation.gql("WHERE Resource_ID = :1",resource_Id)
						final_list = list(list_1)
						count=0
						for item in final_list:
							reserved_from = datetime.datetime.strptime(item.Reservation_from,pattern)
							reserved_till = datetime.datetime.strptime(item.Reservation_endtime,pattern)
							if(Reservation_start > reserved_till) or (Reservation_end < reserved_from ):
						#self.response.write("Reservation Can Be Done")
								count +=1
					
						if(count == len(final_list)):
						#self.response.write("Reservation Can Be Done")
							reserve.put()
							elem = Resource.gql("WHERE Resource_ID =:1",resource_Id)
							counter_list =list(elem)
					#self.response.write(counter_list[0])
							counter_list[0].Resource_counter = datetime.datetime.now()
							counter_list[0].put()
							template_values = {}
			
							template = JINJA_ENVIRONMENT.get_template('Reserve_Success.html')		
							self.response.write(template.render(template_values))
						else:
							self.response.write("Reservation Can not Be Done")
							resource_Id = self.request.get('uid')
							qu = Resource.gql("WHERE Resource_ID = :1",resource_Id)
							new_list = list(qu)
							template_values = { 'error': "The resource is not available. It is already reserved during this time", 'rid' : new_list[0]}
							template = JINJA_ENVIRONMENT.get_template('Reserve_resource.html')
							self.response.write(template.render(template_values))
					else:
						template_values={}
						template = JINJA_ENVIRONMENT.get_template('Cant_Reserve.html')
						self.response.write(template.render(template_values))
					
				else:
					resource_Id = self.request.get('uid')
					qu = Resource.gql("WHERE Resource_ID = :1",resource_Id)
					new_list = list(qu)
					template_values = { 'error': "The resource is not available. Please check resource timings", 'rid' : new_list[0]}
					template = JINJA_ENVIRONMENT.get_template('Reserve_resource.html')
					self.response.write(template.render(template_values))
			
class MainPage(webapp2.RequestHandler):
    def get(self):
        # Checks for active Google account session
        user = users.get_current_user()

        if user:

			#self.response.write(datetime.datetime.now()+ datetime.timedelta(hours = -5))
			#self.response.write(user.nickname())
			url = users.create_logout_url(self.request.uri)
			url_linktext = 'Logout!'
			query1 = Resource.gql("WHERE Resource_owner = :1",user.nickname())
			my_resources = list(query1)
			
			
			query2 = Resource.gql("")
			available = list(query2)
			all_resources = reversed(sorted(available,key=attrgetter('Resource_counter')))
			
			list_1 = []
			current_time = datetime.datetime.now()+ datetime.timedelta(hours = -5)
			query3 = Reservation.gql("WHERE User_ID = :1",user.nickname())
			my_reservations = list(query3)
			for item in my_reservations:
				if(datetime.datetime.strptime(item.Reservation_endtime,'%Y-%m-%d %H:%M') > current_time):
					list_1.append(item)
			my_reservations_list = sorted(list_1,key=attrgetter('Reservation_from'))
			
			template_values = { 'list1' : my_resources, 'list2' : all_resources, 'my_list' : my_reservations_list , 'url' :url , 'url_linktext' : url_linktext}
			template = JINJA_ENVIRONMENT.get_template('main_page.html')
			
			
			self.response.write(template.render(template_values))
			
		   
        else:
			self.redirect(users.create_login_url(self.request.uri))	
	
			
			
class AddResource(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:

			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('add_resource.html')
			self.response.write(template.render(template_values))
		else:
			self.redirect(users.create_login_url(self.request.uri))	
		 
	def post(self):
		user = users.get_current_user()
		initial = '00:00'
		initial_date = datetime.datetime.strptime(initial,'%H:%M')
		resource_key = self.request.get('resource_key')
		greeting = Resource(parent=Resource1_key(resource_key))
		greeting.Resource_name = self.request.get('resource_name')
		greeting.Resource_tags = self.request.get('Tags').split(',')
		greeting.Resource_owner = user.nickname()
		greeting.Resource_start = self.request.get('start')
		greeting.Resource_end = self.request.get('end')
		greeting.Resource_ID = str(uuid.uuid1())
		greeting.Resource_counter = initial_date
		greeting.Resource_description = self.request.get('description')

		values = self.request.get("start")
		value1 = self.request.get("end")
		
		try:
			time.strptime(values,'%H:%M')
			
		except ValueError:
			template_values = { 'error': "The start time is invalid" }
			template = JINJA_ENVIRONMENT.get_template('add_resource.html')
			self.response.write(template.render(template_values))
			return
		try:
			time.strptime(value1,'%H:%M')
		except ValueError:
			template_values = { 'error': "The end time is invalid"}
			template = JINJA_ENVIRONMENT.get_template('add_resource.html')
			self.response.write(template.render(template_values))
			return
		if time.strptime(values,'%H:%M') < time.strptime(value1,'%H:%M'):
			greeting.put()
			template_values = {}
			
			template = JINJA_ENVIRONMENT.get_template('Success.html')
			self.response.write(template.render(template_values))
			
			
		else:
			template_values = { 'error': "The start time is greater than the end time"}
			template = JINJA_ENVIRONMENT.get_template('add_resource.html')
			self.response.write(template.render(template_values))
			
class tag_handler(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				Tag_name = self.request.get('Tag_name')
				resource_list = Resource.gql("WHERE Resource_tags =:1",Tag_name)
				
				tag_list = list(resource_list)
				#self.response.write(tag_list)
				template_values = {'tag_list':tag_list , 'tagname' : Tag_name}
				template = JINJA_ENVIRONMENT.get_template('tags.html')
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))			



class user_handler(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				User_name = self.request.get('User_name')
				user_resource = Resource.gql("WHERE Resource_owner =:1",User_name)
				resources = list(user_resource)
				user_reserve = Reservation.gql("WHERE User_ID =:1",User_name)
				reserves = list(user_reserve)
				template_values = { 'list1' : resources, 'list2' : reserves ,'username' : User_name}
				template = JINJA_ENVIRONMENT.get_template('users.html')
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))	
				
class delete_reservation(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				reservation_id = self.request.get('Res_ID')
				res_details = Reservation.gql("WHERE Reservation_ID =:1",reservation_id)
				details = list(res_details)
				details[0].key.delete()
				template_values = {}
				template = JINJA_ENVIRONMENT.get_template('delete.html')
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))
				
				
class edit_Resource(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			Resource_Id = self.request.get('Resource_ID')
			ls = Resource.gql("WHERE Resource_ID =:1",Resource_Id)
			edit_list = list(ls)
			owner = edit_list[0].Resource_owner

			list2 = ""
			
			for i in range(len(edit_list[0].Resource_tags)):
				list2 = list2 + edit_list[0].Resource_tags[i]
				if not i==len(edit_list[0].Resource_tags)-1:
					list2 = list2 + ","
			
			if(user.nickname() == owner):
					
				template_values = { 'list1' : edit_list[0], 
									'list2' : list2
									}
				template = JINJA_ENVIRONMENT.get_template('edit.html')
				self.response.write(template.render(template_values))
			else:
				template_values={}
				template = JINJA_ENVIRONMENT.get_template('CantEdit.html')
				self.response.write(template.render(template_values))
					
		else:
			self.redirect(users.create_login_url(self.request.uri))
		
	def post(self):
		
		user = users.get_current_user()
		resourceID = self.request.get('Resource_ID')
		
		ls = Resource.gql("WHERE Resource_ID =:1",resourceID)
		edit_list = list(ls)
		
		edit_list[0].Resource_name = self.request.get('r_name')
		edit_list[0].Resource_tags = self.request.get('Tags').split(',')
		edit_list[0].Resource_start = self.request.get('start')
		edit_list[0].Resource_end = self.request.get('end')
		edit_list[0].Resource_description = self.request.get('description')
		
		start_value = self.request.get('start')
		end_value = self.request.get('end')
		
		try:
			time.strptime(start_value,'%H:%M')
			
		except ValueError:
			template_values = { 'error': "The start time is invalid", 'list1' : edit_list[0] }
			template = JINJA_ENVIRONMENT.get_template('edit.html')
			self.response.write(template.render(template_values))
			return
		try:
			time.strptime(end_value,'%H:%M')
		except ValueError:
			template_values = { 'error': "The end time is invalid" , 'list1' : edit_list[0]}
			template = JINJA_ENVIRONMENT.get_template('edit.html')
			self.response.write(template.render(template_values))
			return
		if time.strptime(start_value,'%H:%M') < time.strptime(end_value,'%H:%M'):
			edit_list[0].put()
			template_values = {'newname':edit_list[0].Resource_name}
			template = JINJA_ENVIRONMENT.get_template('Edit_Success.html')
			self.response.write(template.render(template_values))
			
		else:
			template_values = { 'error': "The start time is greater than the end time",'list1' : edit_list[0]}
			template = JINJA_ENVIRONMENT.get_template('edit.html')
			self.response.write(template.render(template_values))


class search_Handler(webapp2.RequestHandler):
	def post(self):
		user=users.get_current_user()
		if user:
			name = self.request.get('resource_name')
			search_list = Resource.gql("WHERE Resource_name =:1",name)
			final_search_list =list(search_list)
			
			
			template_values = { 'list1' : final_search_list , 'name' :name }
			template = JINJA_ENVIRONMENT.get_template('search.html')
			self.response.write(template.render(template_values))			
			
		else:
			self.redirect(users.create_login_url(self.request.uri))			
	
class rss_handler(webapp2.RequestHandler):
		def get(self):
			user = users.get_current_user()
			if user:
				Res_ID = self.request.get('Resource_ID')
				reservation_list = Reservation.gql("WHERE Resource_ID =:1",Res_ID)
				res_list = list(reservation_list)
				Res_name = Resource.gql("WHERE Resource_ID =:1",Res_ID)
				list_1=list(Res_name)
				Resource_name = list_1[0].Resource_name

				template_values = { 'list1' : res_list ,'rname' : Resource_name}
				template = JINJA_ENVIRONMENT.get_template('rss.html')
				self.response.write(template.render(template_values))
			else:
				self.redirect(users.create_login_url(self.request.uri))	

class search_by_availability(webapp2.RequestHandler): 	
	def get(self):
		user = users.get_current_user()
		if user:

			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('Search_byavail.html')
			self.response.write(template.render(template_values))
		else:
			self.redirect(users.create_login_url(self.request.uri))				
	def post(self):
	
		start_time = self.request.get('start_Time')
		pattern = '%Y-%m-%d %H:%M'
		duration = self.request.get('duration')
		try:
			start = datetime.datetime.strptime(start_time,pattern)
			dur = datetime.datetime.strptime(duration,'%H:%M')	
			start_date =datetime.datetime.strptime(start_time,pattern).date()			
			t = datetime.timedelta(minutes = dur.minute) + datetime.timedelta(hours =dur.hour)				
			end_time1 = start + t				
			end_time = end_time1.strftime(pattern)	
		except ValueError:

			template_values = { 'error': "The start time/duration is invalid" }
			template = JINJA_ENVIRONMENT.get_template('Search_byavail.html')
			self.response.write(template.render(template_values))
			return				
		
		resources_list = []
		end_1 = datetime.datetime.strptime(end_time,pattern).date()
		
		if(end_1 > start_date):
			template_values = { 'error': "The reservation cannot span a day boundary" }
			template = JINJA_ENVIRONMENT.get_template('Search_byavail.html')
			self.response.write(template.render(template_values))
		else:
			Reservation_start = datetime.datetime.strptime(start_time,pattern)
			Reservation_end = datetime.datetime.strptime(end_time,pattern)
			current_time = datetime.datetime.now()+ datetime.timedelta(hours = -5)
			Res_start1 = datetime.datetime.strptime(start_time,pattern).time()
			Res_end1 = datetime.datetime.strptime(end_time,pattern).time()
			resource_1 = Resource.gql("")
			list_1 = list(resource_1)	
			count=0
			for item in list_1:
				Resourcestart = datetime.datetime.strptime(item.Resource_start,'%H:%M').time()
				Resourceend = datetime.datetime.strptime(item.Resource_end,'%H:%M').time()
			
			 
				if(Resourcestart <= Res_start1) and (Resourceend >= Res_end1) and (Reservation_start >= current_time):
					list_1 = Reservation.gql("WHERE Resource_ID = :1",item.Resource_ID)
					final_list = list(list_1)
					count=0
					for items in final_list:
						reserved_from = datetime.datetime.strptime(items.Reservation_from,pattern)
						reserved_till = datetime.datetime.strptime(items.Reservation_endtime,pattern)
						if(Reservation_start > reserved_till) or (Reservation_end < reserved_from ):
							count +=1
					
					if(count == len(final_list)):
						resources_list.append(item)
			if(len(resources_list) > 0):
				template_values = { 'list1' : resources_list}			
				template = JINJA_ENVIRONMENT.get_template('search_result.html')		
				self.response.write(template.render(template_values))		
			else:
				template_values = {}
				template = JINJA_ENVIRONMENT.get_template('No_Resources.html')		
				self.response.write(template.render(template_values))
				
app = webapp2.WSGIApplication([
    ('/', MainPage),
	('/add',AddResource),
	('/resource',Resource_handler),
	('/reserve',Reservation_handler),
	('/tags',tag_handler),
	('/user_handle',user_handler),
	('/delete',delete_reservation),
	('/edit',edit_Resource),
	('/search',search_Handler),
	('/rss',rss_handler),
	('/search_1',search_by_availability)
], debug=True)